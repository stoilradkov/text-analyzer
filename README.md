# Text analyzer
A simple tool for analyzing text files,written in Java, currently supporting only .txt extensions.

## How to use:
1.Build project with Maven

Run `mvn clean install` in the home directory of the project

2.Run the genereated jar with parameter the full path of the file you want to analyze
- `java -jar target\TextAnalyzer-0.0.1-SNAPSHOT.jar D:\Example\file.txt` 
## Functionalities:
- Total number of words - displays the total number of words in the file
- Word usage ranking - displays a ranking constisting of words and their usage percentage rounded to the first decimal with the most used word being at the top
- Most frequently used word - displays the most frequently used word in the file or "The file is empty" message if the given file is empty
- Longest word - displays the longest word in the text or "The file is empty" message if the given file is empty
- Total number of characters - displays the total number of characters in the text (excluding whitespaces)
- The number of English words - displays the number of words found in an English dictionary, which can be found [here](https://github.com/dwyl/english-words/blob/master/words_alpha.txt)
- Number of unique words - displays the number of unique words in the file