package options;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class LongestWordOptionTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	private File tempFile;
	private LongestWordOption longestWordOption;

	@Before
	public void initialize() throws IOException {
		tempFile = tempFolder.newFile("testFile.txt");
		longestWordOption = new LongestWordOption();
	}

	private void writeToTempFile(String data) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
		writer.append(data);
		writer.flush();
		writer.close();
	}

	@Test
	public void givenAnEmptyFileWhenExecuteOptionInvokedReturnDefaultMessage() throws IOException {
		assertEquals("The file is empty", longestWordOption.executeOption(tempFile.getAbsolutePath()));
	}

	@Test
	public void givenASingleWordFileWhenExecuteOptionInvokedReturnTheWord() throws IOException {
		writeToTempFile("word");
		assertEquals("Longest word - word", longestWordOption.executeOption(tempFile.getAbsolutePath()));
	}

	@Test
	public void givenAFileWithTwoWordsWithTheSameLengthWhenExecuteOptionInvokedReturnTheFirstOne()
			throws IOException {
		writeToTempFile("word\nmost");
		assertEquals("Longest word - word", longestWordOption.executeOption(tempFile.getAbsolutePath()));
	}

	@Test
	public void givenANonEmptyFileWhenExecuteOptionInvokedReturnTheLongestWord()
			throws IOException {
		writeToTempFile("first word\nsecond second\nlongest word");
		assertEquals("Longest word - longest", longestWordOption.executeOption(tempFile.getAbsolutePath()));
	}
	
	@Test
	public void givenAFileWithEmptyLinesWhenExecuteOptionInvokedShouldIgnoreEmptyLines()
			throws IOException {
		writeToTempFile("\n\n\n");
		assertEquals("The file is empty", longestWordOption.executeOption(tempFile.getAbsolutePath()));
	}

}
