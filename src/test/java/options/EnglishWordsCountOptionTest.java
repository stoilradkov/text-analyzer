package options;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class EnglishWordsCountOptionTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	private File tempFile;
	private EnglishWordsCountOption englishWordsCountOption;

	@Before
	public void initialize() throws IOException {
		tempFile = tempFolder.newFile("testFile.txt");
		englishWordsCountOption = new EnglishWordsCountOption();
	}

	private void writeToTempFile(String data) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
		writer.append(data);
		writer.flush();
		writer.close();
	}

	@Test
	public void givenAnEmptyFileWhenExecuteOptionInvokedReturnZero() throws IOException {
		assertEquals("Number of English words - 0", englishWordsCountOption.executeOption(tempFile.getAbsolutePath()));
	}

	@Test
	public void givenAFileWithAnEnglishWordWhenExecuteOptionInvokedReturnOne() throws IOException {
		writeToTempFile("word");
		assertEquals("Number of English words - 1", englishWordsCountOption.executeOption(tempFile.getAbsolutePath()));
	}

	@Test
	public void givenAFileWithNonEnlgishWordWhenExecuteOptionInvokedReturnZero() throws IOException {
		writeToTempFile("asd");
		assertEquals("Number of English words - 0", englishWordsCountOption.executeOption(tempFile.getAbsolutePath()));
	}

	@Test
	public void givenAFileWithEnglishWordsWhenExecuteOptionInvokedReturnTheNumberOfEnglishWords() throws IOException {
		writeToTempFile("first\n\n second\n third");
		assertEquals("Number of English words - 3", englishWordsCountOption.executeOption(tempFile.getAbsolutePath()));
	}

}
