package options;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class WordCountOptionTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	private File tempFile;
	private WordCountOption wordCountOption;
	
	
	@Before 
	public void initialize() throws IOException {
		tempFile = tempFolder.newFile("testFile.txt");
		wordCountOption = new WordCountOption();
	}
	
	private void writeToTempFile(String data) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
		writer.append(data);
		writer.flush();
		writer.close();	
	}
	
	@Test
	public void givenAnEmptyFileWhenExecuteOptionInvokedReturnZero() throws IOException {
		assertEquals("Total number of words - 0",wordCountOption.executeOption(tempFile.getAbsolutePath()));
	}
	
	@Test
	public void givenASingleWordFileWhenExecuteOptionInvokedReturnOne() throws IOException {
		writeToTempFile("word");
		assertEquals("Total number of words - 1",wordCountOption.executeOption(tempFile.getAbsolutePath()));
	}
	
	@Test
	public void givenAFileWithEmptyLinesWhenExecuteOptionInvokedReturnTheCountOfNonEmptyWords() throws IOException {
		writeToTempFile("word\n\nsecond");
		assertEquals("Total number of words - 2",wordCountOption.executeOption(tempFile.getAbsolutePath()));
	}

}
