package options;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class WordUsageOptionTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	private File tempFile;
	private WordUsageOption wordUsageOption;

	@Before
	public void initialize() throws IOException {
		tempFile = tempFolder.newFile("testFile.txt");
		wordUsageOption = new WordUsageOption();
	}

	private void writeToTempFile(String data) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
		writer.append(data);
		writer.flush();
		writer.close();
	}

	@Test
	public void givenAnEmptyFileWhenExecuteOptionInvokedReturnEmptyString() throws IOException {
		assertEquals("The word ranking is\n", wordUsageOption.executeOption(tempFile.getAbsolutePath()));
	}

	@Test
	public void givenASingleWordFileWhenExecuteOptionInvokedReturnOneEntry() throws IOException {
		writeToTempFile("word");
		assertEquals("The word ranking is\n1. word - 100.0%\n", wordUsageOption.executeOption(tempFile.getAbsolutePath().trim()));
	}

	@Test
	public void givenAFileWithTwoWordsOccurringOnceWhenExecuteOptionInvokedThePercentageShouldBeTheSame()
			throws IOException {
		writeToTempFile("first second");
		assertEquals("The word ranking is\n1. first - 50.0%\n2. second - 50.0%\n",
				wordUsageOption.executeOption(tempFile.getAbsolutePath()));
	}

	@Test
	public void givenAFileWithTwoWordsOneOccurringMoreThanTheOtherWhenExecuteOptionInvokedThePercentagesShouldBeDifferent()
			throws IOException {
		writeToTempFile("first second first");
		assertEquals("The word ranking is\n1. first - 66.7%\n2. second - 33.3%\n",
				wordUsageOption.executeOption(tempFile.getAbsolutePath()));
	}

	@Test
	public void givenAFileWithEmptyLinesWhenExecuteOptionInvokedReturnEntriesWithoutEmptyLine() throws IOException {
		writeToTempFile("first\nsecond\n\nthird");
		assertEquals("The word ranking is\n1. first - 33.3%\n2. second - 33.3%\n3. third - 33.3%\n",
				wordUsageOption.executeOption(tempFile.getAbsolutePath()));
	}

}
