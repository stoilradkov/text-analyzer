package options;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class MostFrequentWordOptionTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	private File tempFile;
	private MostFrequentWordOption mostFrequentWordOption;
	
	
	@Before 
	public void initialize() throws IOException {
		tempFile = tempFolder.newFile("testFile.txt");
		mostFrequentWordOption = new MostFrequentWordOption();
	}
	
	private void writeToTempFile(String data) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
		writer.append(data);
		writer.flush();
		writer.close();	
	}
	
	@Test
	public void givenAnEmptyFileWhenExecuteOptionInvokedReturnDefaultMessage() throws IOException {
		assertEquals("The file is empty",mostFrequentWordOption.executeOption(tempFile.getAbsolutePath()));
	}
	
	@Test
	public void givenASingleWordFileWhenExecuteOptionInvokedReturnTheWord() throws IOException {
		writeToTempFile("word");
		assertEquals("Most frequently used word - word",mostFrequentWordOption.executeOption(tempFile.getAbsolutePath()));
	}
	
	@Test
	public void givenAFileWithTwoWordsOccurringTheSameAmountOfTimesWhenExecuteOptionInvokedReturnTheFirstOne() throws IOException {
		writeToTempFile("word\n\nsecond");
		assertEquals("Most frequently used word - word",mostFrequentWordOption.executeOption(tempFile.getAbsolutePath()));
	}
	
	@Test
	public void givenAFileWithTwoWordsSecondOccurringMoreThanTheOtherWhenExecuteOptionInvokedReturnTheSecondOne() throws IOException {
		writeToTempFile("word\nsecond second");
		assertEquals("Most frequently used word - second",mostFrequentWordOption.executeOption(tempFile.getAbsolutePath()));
	}
}
