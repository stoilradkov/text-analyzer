package options;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class UniqueWordsCountOptionTest {
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	private File tempFile;
	private UniqueWordsCountOption uniqueWordsCountOption;
	
	
	@Before 
	public void initialize() throws IOException {
		tempFile = tempFolder.newFile("testFile.txt");
		uniqueWordsCountOption = new UniqueWordsCountOption();
	}
	
	private void writeToTempFile(String data) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
		writer.append(data);
		writer.flush();
		writer.close();	
	}
	
	@Test
	public void givenAnEmptyFileWhenExecuteOptionInvokedReturnZero() throws IOException {
		assertEquals("Number of unique words - 0",uniqueWordsCountOption.executeOption(tempFile.getAbsolutePath()));
	}
	
	@Test
	public void givenASingleWordFileWhenExecuteOptionInvokedReturnOne() throws IOException {
		writeToTempFile("word");
		assertEquals("Number of unique words - 1",uniqueWordsCountOption.executeOption(tempFile.getAbsolutePath()));
	}
	
	@Test
	public void givenAFileWithTheSameTwoWordsWhenExecuteOptionInvokedReturnOne() throws IOException {
		writeToTempFile("first\n\nfirst");
		assertEquals("Number of unique words - 1",uniqueWordsCountOption.executeOption(tempFile.getAbsolutePath()));
	}
	
	@Test
	public void givenAFileWithDifferentWordsWhenExecuteOptionInvokedReturnTheNumberOfWords() throws IOException {
		writeToTempFile("first\nsecond third");
		assertEquals("Number of unique words - 3",uniqueWordsCountOption.executeOption(tempFile.getAbsolutePath()));
	}
}
