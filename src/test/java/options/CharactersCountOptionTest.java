package options;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class CharactersCountOptionTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	private File tempFile;
	private CharactersCountOption charactersCountOption;
	
	
	@Before 
	public void initialize() throws IOException {
		tempFile = tempFolder.newFile("testFile.txt");
		charactersCountOption = new CharactersCountOption();
	}
	
	private void writeToTempFile(String data) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
		writer.append(data);
		writer.flush();
		writer.close();	
	}
	
	@Test
	public void givenAnEmptyFileWhenExecuteOptionInvokedReturnZero() throws IOException {
		assertEquals("Total number of characters - 0",charactersCountOption.executeOption(tempFile.getAbsolutePath()));
	}
	
	@Test
	public void givenANonEmptyFileWhenExecuteOptionInvokedReturnTotalNumberOfCharacters() throws IOException {
		writeToTempFile("wordsecond");
		assertEquals("Total number of characters - 10",charactersCountOption.executeOption(tempFile.getAbsolutePath()));
	}
	
	@Test
	public void givenAFileWithEmptyLinesWhenExecuteOptionInvokedShouldIgnoreEmptyLines() throws IOException {
		writeToTempFile("word\n\nsecond");
		assertEquals("Total number of characters - 10",charactersCountOption.executeOption(tempFile.getAbsolutePath()));
	}
	
	@Test
	public void givenAfileWithWhiteSpacesWhenExecuteOptionInvokedShouldIgnoreWhiteSpaces() throws IOException {
		writeToTempFile("first second\tthird");
		assertEquals("Total number of characters - 16", charactersCountOption.executeOption(tempFile.getAbsolutePath()));
	}
}
