package application;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import application.OptionDispatcher;
import datavalidation.UserInputValidator;
import options.Option;
import options.OptionsFactory;
import view.Console;
import view.InputOutputType;
import view.OptionsMenu;

public class OptionDispatcherTest {
	private UserInputValidator dataValidator;
	private OptionsFactory optionsFactory;
	private String filePath;
	private OptionsMenu optionsMenu;
	private OptionDispatcher dispatcher;
	
	@Before
	public void initialize() {
		dataValidator = Mockito.mock(UserInputValidator.class);
		optionsFactory = Mockito.mock(OptionsFactory.class);
		filePath = null;
		optionsMenu = Mockito.mock(OptionsMenu.class);
	}
	
	private void createDispatcher(String input) {
		Scanner scanner = new Scanner(new ByteArrayInputStream(input.getBytes()));
		InputOutputType inputOutputType = new Console(scanner);
		dispatcher = new OptionDispatcher(dataValidator, inputOutputType, 
				optionsFactory, filePath, optionsMenu);
	}
	
	private PrintStream checkCondition(String expected) {
		ByteArrayOutputStream contentToBePrinted = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(contentToBePrinted);
		PrintStream oldStream = System.out;
		System.setOut(printStream);
		
		dispatcher.dispatchInput();
		
		System.out.flush();

		assertEquals(expected, contentToBePrinted.toString().trim());
		return oldStream;
	}
	
	@Test
	public void givenExitInputWhenDispatchInvokedExitMethod() {
		createDispatcher("exit");
		PrintStream oldStream = checkCondition("");
		System.setOut(oldStream);
	}
	
	@Test
	public void givenAValidOptionWhenDispatchInvokedDisplayOptionOutput() throws FileNotFoundException, IOException {
		createDispatcher("1\nexit");
		Option option = Mockito.mock(Option.class);
		
		Mockito.when(dataValidator.isNumber("1")).thenReturn(true);
		Mockito.when(optionsFactory.getOption(0)).thenReturn(option);
		Mockito.when(optionsMenu.getNumberOfAvailableOptions()).thenReturn(1);
		Mockito.when(option.executeOption(filePath)).thenReturn("Correct option");
		
		PrintStream oldStream = checkCondition("Correct option");
		System.setOut(oldStream);
	}
	
	@Test
	public void givenAnOptionBiggerThanSupportedNumberOfOptionsWhenDispatchInvokedDisplayDefaultMessage() throws FileNotFoundException, IOException {
		createDispatcher("2\nexit");
		
		Mockito.when(dataValidator.isNumber("2")).thenReturn(true);
		Mockito.when(optionsMenu.getNumberOfAvailableOptions()).thenReturn(1);
		
		PrintStream oldStream = checkCondition("Please select a valid option");
		System.setOut(oldStream);
	}
	
	@Test
	public void givenAnOptionInAnInvalidRangeWhenDispatchInvokedDisplayDefaultMessage() throws FileNotFoundException, IOException {
		createDispatcher("-1\nexit");
		
		Mockito.when(dataValidator.isNumber("-1")).thenReturn(true);
		Mockito.when(optionsMenu.getNumberOfAvailableOptions()).thenReturn(1);
		
		PrintStream oldStream = checkCondition("Please select a valid option");
		System.setOut(oldStream);
	}
	
	@Test
	public void givenAnOptionThatIsNotANumberWhenDispatchInvokedDisplayDefaultMessage() {
		createDispatcher("notANumber\nexit");
		
		Mockito.when(dataValidator.isNumber("notANumber")).thenReturn(false);
		PrintStream oldStream = checkCondition("Please select a valid option");
		System.setOut(oldStream);
	}
}
