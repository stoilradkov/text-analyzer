package datavalidation;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class UserInputValidatorTest {

	private UserInputValidator dataValidator;
	private Set<String> supportedFileExtensions;
	
	@Before
	public void initialize() {
		supportedFileExtensions = new HashSet<>();
		supportedFileExtensions.add("txt");
		dataValidator = new UserInputValidator(supportedFileExtensions);
	}
	
	@Test
	public void givenTwoArgumentsWhenIsValidFilePathInvokedReturnFalse() {
		String[] args = new String[2];
		args[0] = "first";
		args[1] = "second";

		assertFalse(dataValidator.isValidFilePath(args));

	}

	@Test
	public void givenAnIllegalFilePathWhenIsValidFilePathInvokedReturnFalse() {
		String[] args = new String[1];
		args[0] = "not.a.valid.file.path";
		
		assertFalse(dataValidator.isValidFilePath(args));
		
	}
	
	@Test
	public void givenAnUnsupportedFileExtensionWhenIsValidFilePathInvokedReturnFalse() {
		String[] args = new String[1];
		args[0] = "/file.word";
		
		assertFalse(dataValidator.isValidFilePath(args));
	}
	
	@Test
	public void givenAValidFilePathWhenIsValidPathInvokedReturnTrue() {
		String[] args = new String[1];
		args[0] = "/file.txt";
		
		assertTrue(dataValidator.isValidFilePath(args));
	}
	
	@Test
	public void givenANumberWhenIsNumberInvokedReturnTrue() {
		assertTrue(dataValidator.isNumber("3"));
	}

	@Test
	public void givenAStringThatIsNotANumberWhenIsNumberInvokedReturnFalse() {
		assertFalse(dataValidator.isNumber("not a number"));
	}
	
}
