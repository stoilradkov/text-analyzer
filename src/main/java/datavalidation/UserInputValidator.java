package datavalidation;

import java.util.Set;

public class UserInputValidator {
	public final static int EXTENSION_INDEX = 1;
	public final static int FILENAME_PARTS_LENGTH = 2;
	public final static int EXPECTED_ARGUMENT_COUNT = 1;
	public final static String FILE_DELIMITER = "\\.";

	private Set<String> supportedFileExtensions;

	public UserInputValidator(Set<String> supportedFileExtensions) {
		this.supportedFileExtensions = supportedFileExtensions;
	}

	/**
	 * Checks if the given file is supported
	 * 
	 * @param args the file path given by the user
	 * @return true if the file is supported
	 */
	public boolean isValidFilePath(String[] args) {
		if (args.length != EXPECTED_ARGUMENT_COUNT) {
			return false;
		}

		String[] fileNameParts = args[0].split(FILE_DELIMITER);
		if (fileNameParts.length != FILENAME_PARTS_LENGTH) {
			return false;
		}

		return isValidFileExtension(fileNameParts[EXTENSION_INDEX]);
	}

	/**
	 * Checks if the given string is a number
	 * 
	 * @param inputString the given string
	 * @return true if the string is a number
	 */
	public boolean isNumber(String inputString) {
		try {
			Integer.parseInt(inputString);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	private boolean isValidFileExtension(String extension) {
		return supportedFileExtensions.contains(extension);
	}
}
