package options;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Stream;

public class CharactersCountOption implements Option {

	/**
	 * Finds the total number of non-whitespace characters in the file
	 * 
	 * @param absoluteFilePath the absolute path to the file
	 * @return the total number of non-whitespace characters in the file
	 * 
	 * @throws IOException           if an exception occurs while closing the file
	 *                               reader
	 * @throws FileNotFoundException if the given file path is illegal
	 */
	@Override
	public String executeOption(String absoluteFilePath) throws FileNotFoundException, IOException {
		try (BufferedReader reader = new BufferedReader(new FileReader(absoluteFilePath));) {
			String whiteSpaceRegex = RegExUtil.WHITE_SPACE_REGEX.getRegex();
			
			int totalNumberOfCharacters = reader.lines().filter(line -> !line.isEmpty())
					.flatMap(line -> Stream.of(line.split(whiteSpaceRegex))).mapToInt(String::length).sum();

			return "Total number of characters - " + String.valueOf(totalNumberOfCharacters);
		}
	}
}
