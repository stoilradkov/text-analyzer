package options;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MostFrequentWordOption implements Option {
	
	public final static String DEFAULT_MESSAGE = "The file is empty";

	/**
	 * Finds the most frequently used word in the file or a default message if the
	 * file is empty
	 * 
	 * @param absoluteFilePath the absolute path to the file
	 * @return the most frequently used word
	 * 
	 * @throws IOException           if an exception occurs while closing the file
	 *                               reader
	 * @throws FileNotFoundException if the given file path is illegal
	 */
	@Override
	public String executeOption(String absoluteFilePath) throws FileNotFoundException, IOException {
		try (BufferedReader reader = new BufferedReader(new FileReader(absoluteFilePath));) {
			Map<String, Long> wordsCount = getWordCountMapping(reader);
			String mostFrequentWord = getMostFrequentWord(wordsCount);

			if(mostFrequentWord.equals(DEFAULT_MESSAGE)) {
				return DEFAULT_MESSAGE;
			}
			
			return "Most frequently used word - " + mostFrequentWord;
		}
	}

	/**
	 * Creates a map consisting of a word and the times it occurs in the file
	 * 
	 * @param reader the reader for the file
	 * @return a map with the words and the number of times they occur in the file
	 */
	private Map<String, Long> getWordCountMapping(BufferedReader reader) {
		String wordRegex = RegExUtil.WORD_REGEX.getRegex();
		
		Map<String, Long> wordsCount = reader.lines().filter(line -> !line.isEmpty())
				.flatMap(line -> Stream.of(line.split(wordRegex)))
				.collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()));

		return wordsCount;
	}

	/**
	 * Finds the word with the biggest value count from the given map or a default
	 * message if the map is empty
	 * 
	 * @param wordsCount the map from consisting of word - occurrences pairs
	 * @return the most frequently seen word in the map or default message if the
	 *         map is empty
	 */

	private String getMostFrequentWord(Map<String, Long> wordsCount) {
		String mostFrequentWord = wordsCount.entrySet().stream()
				.max((wordEntry1, wordEntry2) -> wordEntry1.getValue().compareTo(wordEntry2.getValue()))
				.map(wordEntry -> wordEntry.getKey()).orElse(DEFAULT_MESSAGE);

		return mostFrequentWord;
	}
}
