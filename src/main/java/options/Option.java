package options;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface Option {

	public String executeOption(String FilePath) throws FileNotFoundException, IOException ;
}
