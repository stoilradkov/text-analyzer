package options;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnglishWordsCountOption implements Option {
	public final static Logger logger = LoggerFactory.getLogger(EnglishWordsCountOption.class);

	public final static String DICTIONARY_PATH = "src/main/resources/dictionary.txt";
	
	/**
	 * Finds the total number of english words in the file
	 * 
	 * @param absoluteFilePath the absolute path to the file
	 * @return the total number of non-whitespace characters in the file
	 * 
	 * @throws IOException           if an exception occurs while closing the file
	 *                               reader
	 * @throws FileNotFoundException if the given file path is illegal
	 */
	@Override
	public String executeOption(String absoluteFilePath) throws FileNotFoundException, IOException {
		try (BufferedReader reader = new BufferedReader(new FileReader(absoluteFilePath));) {
			String wordRegex = RegExUtil.WORD_REGEX.getRegex();
			
			long totalEnglishWordsCount = reader.lines().filter(line -> !line.isEmpty())
					.flatMap(line -> Stream.of(line.split(wordRegex))).filter(word -> isFoundInDictionary(word)).count();

			return "Number of English words - " + String.valueOf(totalEnglishWordsCount);
		}
	}

	/**
	 * Checks if the given word is found in a dictionary
	 * 
	 * @param word the given word
	 * @return true if the word is found in a dictionary
	 */

	private boolean isFoundInDictionary(String word) {
		try (BufferedReader reader = new BufferedReader(new FileReader(DICTIONARY_PATH));) {
			boolean isFound = reader.lines().anyMatch(dictionaryWord -> dictionaryWord.equalsIgnoreCase(word));

			return isFound;
		} catch (FileNotFoundException e) {
			logger.error("File not found " + e.getMessage());
		} catch (IOException e) {
			logger.error("An error occurred while closing the reader " + e.getMessage());
		}

		return false;
	}
}
