package options;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Stream;

public class UniqueWordsCountOption implements Option {
	/**
	 * Finds the number of unique words in the file
	 * 
	 * @param absoluteFilePath the absolute path to the file
	 * @return the total number of unique words in the file
	 * 
	 * @throws IOException           if an exception occurs while closing the file
	 *                               reader
	 * @throws FileNotFoundException if the given file path is illegal
	 */
	@Override
	public String executeOption(String absoluteFilePath) throws FileNotFoundException, IOException {
		String wordRegex = RegExUtil.WORD_REGEX.getRegex();
		
		try (BufferedReader reader = new BufferedReader(new FileReader(absoluteFilePath));) {
			long numberOfUniqueWords = reader.lines().filter(line -> !line.isEmpty())
					.flatMap(line -> Stream.of(line.split(wordRegex))).distinct().count();
			
			return "Number of unique words - " + String.valueOf(numberOfUniqueWords);
		}
	}
}
