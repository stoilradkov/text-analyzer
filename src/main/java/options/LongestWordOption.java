package options;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Stream;

public class LongestWordOption implements Option {
	
	public final static String DEFAULT_MESSAGE = "The file is empty";
	/**
	 * Finds the longest word in the file or a default message if the file is empty
	 * 
	 * @param absoluteFilePath the absolute path to the file
	 * @return the longest word in the file
	 * 
	 * @throws IOException           if an exception occurs while closing the file
	 *                               reader
	 * @throws FileNotFoundException if the given file path is illegal
	 */
	@Override
	public String executeOption(String absoluteFilePath) throws FileNotFoundException, IOException {
		try (BufferedReader reader = new BufferedReader(new FileReader(absoluteFilePath));) {
			String wordRegex = RegExUtil.WORD_REGEX.getRegex();
			
			String longestWord = reader.lines().filter(line -> !line.isEmpty())
					.flatMap(line -> Stream.of(line.split(wordRegex)))
					.max((String word1, String word2) -> word1.length() - word2.length()).orElse(DEFAULT_MESSAGE);

			if (longestWord.equals(DEFAULT_MESSAGE)) {
				return DEFAULT_MESSAGE;
			}
			return "Longest word - " + longestWord;
		}
	}
}
