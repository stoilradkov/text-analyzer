package options;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordUsageOption implements Option {

	public final static int PERCENTAGE_COEFFICIENT = 100;
	public final static int ROUNDING_PlACES = 1;

	/**
	 * Calculates a ranking of the words based on usage in the text
	 * 
	 * @param absoluteFilePath the absolute path to the file
	 * @return a ranking of the words based on usage
	 * 
	 * @throws IOException           if an exception occurs while closing the file
	 *                               reader
	 * @throws FileNotFoundException if the given file path is illegal
	 */
	@Override
	public String executeOption(String absoluteFilePath) throws FileNotFoundException, IOException {
		try (BufferedReader reader = new BufferedReader(new FileReader(absoluteFilePath));) {
			Map<String, Long> unsortedWordsCount = getWordCountMapping(reader);
			Map<String, Long> sortedWords = sortByValue(unsortedWordsCount);

			Set<Entry<String, Long>> wordEntries = sortedWords.entrySet();

			long totalNumberOfWords = getTotalNumberOfWords(unsortedWordsCount);
			StringBuilder response = new StringBuilder();
			int wordRank = 1;
			
			for (Entry<String, Long> word : wordEntries) {
				double roundedPercentage = getOccurrencePercentage(word, totalNumberOfWords);

				response.append(wordRank + ". " + word.getKey() + " - " + roundedPercentage + "%\n");

				wordRank++;
			}

			return "The word ranking is\n" + response.toString();
		}
	}

	/**
	 * Calculates the percentage of word occurrences rounded to the first decimal
	 * 
	 * @param word the given word
	 * @param totalNumberOfWords the total number of words
	 * 
	 * @return the percentage of occurrence rounded to the first decimal
	 */
	private double getOccurrencePercentage(Entry<String, Long> word, long totalNumberOfWords) {
		double occurrencePercentage = word.getValue().doubleValue() * PERCENTAGE_COEFFICIENT
				/ totalNumberOfWords;
		
		BigDecimal bd = new BigDecimal(occurrencePercentage);
    	bd = bd.setScale(ROUNDING_PlACES, RoundingMode.HALF_UP);
		double roundedToFirstDecimal = bd.doubleValue();
		
		return roundedToFirstDecimal;
	}

	/**
	 * Creates a map consisting of a word and the times it occurs in the file
	 * 
	 * @param reader the reader for the file
	 * @return a map with the words and the number of times they occur in the file
	 */
	private Map<String, Long> getWordCountMapping(BufferedReader reader) {
		String wordRegex = RegExUtil.WORD_REGEX.getRegex();

		Map<String, Long> wordsCount = reader.lines().filter(line -> !line.isEmpty())
				.flatMap(line -> Stream.of(line.split(wordRegex)))
				.collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()));

		return wordsCount;
	}

	/**
	 * Finds the total number of words from the map
	 * 
	 * @param wordsCount the map consisting of word - number of occurrences pair
	 * @return the total number of words
	 */
	private long getTotalNumberOfWords(Map<String, Long> wordsCount) {
		long totalNumberOfWords = wordsCount.values().stream().mapToLong(Long::valueOf).sum();

		return totalNumberOfWords;
	}

	/**
	 * Sorts the given map by value in descending order
	 * 
	 * @param unsortedWordsCount the map that should be sorted
	 * @return the sorted by value map
	 */

	private Map<String, Long> sortByValue(Map<String, Long> unsortedWordsCount) {
		Map<String, Long> sortedWordsCount = unsortedWordsCount.entrySet().stream()
				.sorted(Map.Entry.<String, Long>comparingByValue().reversed())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

		return sortedWordsCount;
	}

}
