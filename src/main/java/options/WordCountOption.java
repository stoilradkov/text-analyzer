package options;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class WordCountOption implements Option {

	/**
	 * Calculates the total number of the words in the given file
	 * 
	 * @param absoluteFilePath the absolute path to the file
	 * @return the number of words as a string
	 * 
	 * @throws IOException           if an exception occurs while closing the file
	 *                               reader
	 * @throws FileNotFoundException if the given file path is illegal
	 */
	@Override
	public String executeOption(String absoluteFilePath) throws FileNotFoundException, IOException {
		try (BufferedReader reader = new BufferedReader(new FileReader(absoluteFilePath));) {
			long wordCount = reader.lines().filter(line -> !line.isEmpty()).mapToInt(line -> getNumberOfWords(line))
					.sum();

			return "Total number of words - " + String.valueOf(wordCount);
		}
	}

	/**
	 * Returns the number of words in the given line
	 * 
	 * @param line the given line
	 * @return the number of words in the line
	 */

	private int getNumberOfWords(String line) {
		String wordRegex = RegExUtil.WORD_REGEX.getRegex();
		
		return line.split(wordRegex).length;
	}

}
