package options;

public enum RegExUtil {
	WORD_REGEX("\\W+"),WHITE_SPACE_REGEX("\\s+");
	
	private final String regex;
	
	private RegExUtil(String regex) {
		this.regex = regex;
	}
	
	public String getRegex() {
		return this.regex;
	}
}
