package options;

import java.util.List;

public class OptionsFactory {

	private List<Option> options;

	public OptionsFactory(List<Option> options) {
		this.options = options;
	}

	public Option getOption(int optionIndex) {
		return options.get(optionIndex);
	}
}
