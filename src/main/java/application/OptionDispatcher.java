package application;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import datavalidation.UserInputValidator;
import options.Option;
import options.OptionsFactory;
import view.InputOutputType;
import view.OptionsMenu;

public class OptionDispatcher {
	public final static Logger logger = LoggerFactory.getLogger(OptionDispatcher.class);

	public final static String EXIT_CONDITION = "exit";

	private UserInputValidator dataValidator;
	private InputOutputType inputOutputType;
	private OptionsFactory optionsFactory;
	private String filePath;
	private OptionsMenu optionsMenu;

	public OptionDispatcher(UserInputValidator dataValidator, InputOutputType inputOutputType,
			OptionsFactory optionsFactory, String filePath, OptionsMenu optionsMenu) {
		this.dataValidator = dataValidator;
		this.inputOutputType = inputOutputType;
		this.optionsFactory = optionsFactory;
		this.filePath = filePath;
		this.optionsMenu = optionsMenu;
	}

	/**
	 * Takes the user input and displays the output of the selected option
	 */

	public void dispatchInput() {
		optionsMenu.displayAvailableOptions();

		StringBuilder inputData = new StringBuilder();
		inputData.replace(0, inputData.length(), inputOutputType.readLine());

		StringBuilder outputData = new StringBuilder();
		while (!inputData.toString().equals(EXIT_CONDITION)) {
			String optionResponse = "An error occurred.Try again";
			try {
				optionResponse = invokeOption(inputData.toString());
			} catch (FileNotFoundException e) {
				logger.warn("File not found " + e.getMessage());
				inputOutputType.display("The file could not be found.The program will now exit");

				return;
			} catch (IOException e) {
				logger.error("An error occurred while closing the reader " + e.getMessage());
			}

			outputData.replace(0, outputData.length(), optionResponse);
			inputOutputType.display(outputData.toString());

			optionsMenu.displayAvailableOptions();
			inputData.replace(0, inputData.length(), inputOutputType.readLine());
		}
	}

	/**
	 * Takes the user input and invokes the correct option
	 * 
	 * @param inputData the user input
	 * @return returns the option output or a default message if the input is
	 *         illegal
	 * 
	 * @throws IOException           if an exception occurs while closing the file
	 *                               reader
	 * @throws FileNotFoundException if the given file path is illegal
	 */
	private String invokeOption(String inputData) throws FileNotFoundException, IOException {
		if (isLegalData(inputData)) {
			int optionIndex = Integer.parseInt(inputData);
			Option option = optionsFactory.getOption(optionIndex - 1);
			String output = option.executeOption(filePath);

			return output;
		}

		return "Please select a valid option";
	}

	/**
	 * Checks if the given input data is legal i.e. it is a number and the number
	 * corresponds to an existing option
	 * 
	 * @param inputData the user input data
	 * @return true if the data is legal
	 */

	private boolean isLegalData(String inputData) {
		if (dataValidator.isNumber(inputData)) {
			int optionIndex = Integer.parseInt(inputData);
			if (1 <= optionIndex && optionIndex <= optionsMenu.getNumberOfAvailableOptions()) {
				return true;
			}
			return false;
		}
		return false;
	}

}
