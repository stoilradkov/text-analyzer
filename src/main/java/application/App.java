package application;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import datavalidation.UserInputValidator;
import options.CharactersCountOption;
import options.EnglishWordsCountOption;
import options.LongestWordOption;
import options.MostFrequentWordOption;
import options.Option;
import options.OptionsFactory;
import options.UniqueWordsCountOption;
import options.WordCountOption;
import options.WordUsageOption;
import view.InputOutputType;
import view.OptionsMenu;
import view.Console;

public class App {
	public static void main(String[] args) {
		App application = new App();
		InputOutputType inputOutput = application.initializeConsoleInputOutputType();

		UserInputValidator dataValidator = application.initializeDataValidator();
		if (!dataValidator.isValidFilePath(args)) {
			inputOutput.display("Malformed file path.The app will now exit");
			return;
		}

		OptionsFactory optionsFactory = application.initializeOptionFactory();

		OptionsMenu optionsMenu = application.initializeOptionsMenu(inputOutput);

		OptionDispatcher dispatcher = new OptionDispatcher(dataValidator, inputOutput, optionsFactory, args[0],
				optionsMenu);

		dispatcher.dispatchInput();
	}

	/**
	 * Initializes input - output type with console I/O
	 * 
	 * @return initialized I/O type
	 */

	private InputOutputType initializeConsoleInputOutputType() {
		InputOutputType inputOutput = new Console(new Scanner(System.in));

		return inputOutput;
	}

	/**
	 * Generates a set of supported file extensions
	 * 
	 * @return the set of supported file extensions
	 */
	private Set<String> generateSupportedFileExtensions() {
		Set<String> supportedFileExtensions = new HashSet<>();
		supportedFileExtensions.add("txt");

		return supportedFileExtensions;
	}

	/**
	 * Initializes data validator with supported file extensions
	 * 
	 * @return the initialized data validator
	 */
	private UserInputValidator initializeDataValidator() {
		Set<String> supportedFileExtensions = generateSupportedFileExtensions();

		UserInputValidator dataValidator = new UserInputValidator(supportedFileExtensions);

		return dataValidator;
	}

	/**
	 * Initializes option factory with supported options
	 * 
	 * @return the initialized option factory
	 */
	private OptionsFactory initializeOptionFactory() {
		List<Option> options = new ArrayList<>();
		options.add(new WordCountOption());
		options.add(new WordUsageOption());
		options.add(new MostFrequentWordOption());
		options.add(new LongestWordOption());
		options.add(new CharactersCountOption());
		options.add(new EnglishWordsCountOption());
		options.add(new UniqueWordsCountOption());

		OptionsFactory optionsFactory = new OptionsFactory(options);

		return optionsFactory;
	}

	/**
	 * Initializes options menu with supported options
	 * 
	 * @param inputOutput the I/O type
	 * @return the initialized options menu
	 */

	private OptionsMenu initializeOptionsMenu(InputOutputType inputOutput) {
		List<String> availableOptions = new ArrayList<>();
		availableOptions.add("Total number of words");
		availableOptions.add("Word usage ranking");
		availableOptions.add("Most frequently used word");
		availableOptions.add("Longest word");
		availableOptions.add("Total number of characters");
		availableOptions.add("The number of English words");
		availableOptions.add("Number of unique words");

		OptionsMenu optionsMenu = new OptionsMenu(availableOptions, inputOutput);

		return optionsMenu;
	}
}
