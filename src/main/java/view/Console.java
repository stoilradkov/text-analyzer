package view;

import java.util.Scanner;

public class Console implements InputOutputType{
	private Scanner scanner;
	
	public Console(Scanner scanner) {
		this.scanner = scanner;
	}
	
	@Override
	public String readLine() {
		if(scanner.hasNext()) {
			return scanner.nextLine();			
		}
		
		return "exit";
	}
	
	/**
	 * Prints the output to the console
	 * 
	 * @param output the output to be printed
	 * 
	 */
	@Override
	public void display(String output) {
		System.out.println(output);
	}
	
	
}
