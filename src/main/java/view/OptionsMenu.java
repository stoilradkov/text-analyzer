package view;

import java.util.List;

public class OptionsMenu {
	private List<String> availableOptions;
	private InputOutputType inputOutputType;

	public OptionsMenu(List<String> availableOptions, InputOutputType inputOutputType) {
		this.availableOptions = availableOptions;
		this.inputOutputType = inputOutputType;
	}

	
	/**
	 * Displays the supported options
	 */
	public void displayAvailableOptions() {
		inputOutputType.display("Press any of the numbers corresponding "
				+ "to the options or " + "type exit to exit the app");

		int totalNumberOfOptions = availableOptions.size();
		for (int optionIndex = 0; optionIndex < totalNumberOfOptions; optionIndex++) {
			int optionNumber = optionIndex + 1;
			inputOutputType.display(availableOptions.get(optionIndex) + " - " + optionNumber);
		}
	}
	
	/**
	 * Returns the number of available options
	 * 
	 * @return the number of available options
	 */
	
	public int getNumberOfAvailableOptions() {
		return availableOptions.size();
	}
}
