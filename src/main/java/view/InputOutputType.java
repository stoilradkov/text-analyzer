package view;

public interface InputOutputType {
	public String readLine();

	public void display(String output);
}
